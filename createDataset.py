import contextlib
import io
import os

import pytube
import speech_recognition
import speech_recognition as sr
from pydub import AudioSegment

# TODO Requires ffmpeg installed locally or in the venv

LIOPHY_CHANNEL = pytube.Channel('https://www.youtube.com/user/liophy/videos')
VIDEOS_COUNT = 1  # downloads the last VIDEOS_COUNT videos
LAST_MS = 10000  # saves the last LAST_MS for analysis
NOISE_SUPPRESSION_MS = 8000  # applies noise suppression for up to NOISE_SUPPRESSION_MS
DATASET_DIRECTORY_NAME = "output"

os.makedirs(f".\\{DATASET_DIRECTORY_NAME}\\", exist_ok=True)

video_urls = [video for video in LIOPHY_CHANNEL.video_urls[:VIDEOS_COUNT]]
recognized_speech = []
intermediate_files = []  # TODO OS/ffmpeg/package still keeps files opened, cannot be deleted

for video_url in video_urls:

    # Download audio and isolate last few seconds
    try:
        video = pytube.YouTube(video_url)
        print(f"Downloading: {video.title}")

        date_tokens = video.title.split()[:-2]

        entry_name = '-'.join(date_tokens)
        audio_file_name = f"{entry_name}-audio"
        last10_file_name = f"{entry_name}-last10.mp4"

        audio = video.streams.filter(only_audio=True, file_extension='mp4').first()
        audio.download(filename=audio_file_name)

        audio_segment_mp4 = AudioSegment.from_file(audio_file_name, "mp4")
        intermediate_files.append(audio_file_name)

        last_10_seconds_mp4 = audio_segment_mp4[-LAST_MS:].export(last10_file_name, format="mp4")
        intermediate_files.append(last10_file_name)
    except:
        print("Failed to download audio file")
        break

    # Convert mp4 file to wav
    output_wav_file_name = f".\\{DATASET_DIRECTORY_NAME}\\{entry_name}-wav.wav"
    COMMAND_TO_WAV = f"ffmpeg -i {last10_file_name} -ab 160k -ac 2 -ar 44100 -vn {output_wav_file_name}"

    with contextlib.redirect_stderr(io.StringIO):
        os.system(COMMAND_TO_WAV)

    recording = sr.AudioFile(output_wav_file_name)
    recognizer = sr.Recognizer()

    with recording as source:
        output_speech_file_name = f".\\{DATASET_DIRECTORY_NAME}\\{entry_name}-speech.txt"
        print("Speech recognizer is listening")
        recognizer.adjust_for_ambient_noise(source, duration=NOISE_SUPPRESSION_MS / 1000)
        audio = recognizer.record(source)

        label = open(output_speech_file_name, "w")

        try:
            text = recognizer.recognize_google(audio, language="BG-bg")
            print(f"Extracted from audio : {text}")
        except speech_recognition.UnknownValueError:
            text = ''
            print("Could not recognize audio from recording.")

        label.write(text)
        recognized_speech.append(text)
        label.close()

print("Recognized speech in all videos:")
for entry in recognized_speech:
    print(entry)
